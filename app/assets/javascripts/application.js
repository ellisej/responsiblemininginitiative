//= require jquery
//= require jquery_ujs
//= require jquery.slick
//= require jquery.turbolinks
//= require tether
//= require bootstrap-sprockets
//= require tinymce
//= require cocoon
//= require turbolinks
//= require_tree .

$(document).on('page:receive', function(){
  tinymce.remove();
  tinyMCE.init({
    selector: "textarea.tinymce",
  });
});
