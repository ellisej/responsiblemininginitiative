jQuery ->
    $('.carousel').slick({
        arrows: true,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 800,
        dots: true,
        infinite: true
    })
