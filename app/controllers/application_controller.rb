class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_constants
  
  #For Drop-Down Functionality with the nav-bar header
  def set_constants
    @mining_life_cycle = Page.friendly.find "mining-life-cycle" rescue nil
    @commodity_types = Page.friendly.find "commodity-types" rescue nil
    @mining_issues = Page.friendly.find "mining-issues" rescue nil
    @societal_impacts = Page.friendly.find "societal-impacts" rescue nil
    @regulatory_framework = Page.friendly.find "regulatory-framework" rescue nil
  end

end
