class SuggestionsController < ApplicationController
  before_action :set_suggestion, only: [:edit, :update, :destroy]
  before_action :admin_signed_in?, only: [:index, :new, :edit, :create, :update, :destroy]

  def index
    @suggestions = Suggestion.all
  end

  def new
    @suggestion = Suggestion.new
    @images = Image.where.not(page_id: nil)
    @images = @images.shuffle
  end

  def create
    @suggestion = Suggestion.new(suggestion_params)
    if @suggestion.save
      flash.alert = true #flash variable, prompts feedback to the user that the submission was succesfull
      #redirect_to root_path, notice: 'Suggestion was successfully created!'
      #we redirect back to the suggestion submission page
      redirect_to action: "new"
    else
    	@images = Image.where.not(page_id: nil)
   	@images = @images.shuffle
	render :new, notice: 'Section was not able to be created!'
    end
  end

  def update
    if @suggestion.update(suggestion_params)
      redirect_to @suggestion, notice: 'Suggestion was successfully updated!'
    else
	@images = Image.where.not(page_id: nil)
	@images = @images.shuffle
	render :new, notice: 'Section was not able to be updated!'
    end
  end

  def destroy
    @suggestion.destroy
    @suggestions = Suggestion.all
    render :index
  end

  private
    
    def set_suggestion
      @suggestion = Suggestion.find(params[:id])
    end

    def suggestion_params
      params.require(:suggestion).permit(:content, :email, :name)
    end
end
