class PagesController < ApplicationController
  before_action :set_page, only: [:edit, :update, :destroy]
  before_action :get_images, only: [:home, :suggestions]
  before_action :admin_signed_in?, only: [:admin_panel, :new, :edit, :create, :update, :destroy]

  def home
    @page = Page.friendly.find("responsible-mining-initiative") rescue nil
    @sections = @page.sections.order(:order) rescue nil
    @images = @images.shuffle
  end

  def show
    @page = Page.friendly.find(params[:id])
    @sections = @page.sections.order(:order)
    @images = @page.images
    get_images if @images.empty?
  end


  def admin_panel
    @page = Page.friendly.find(params[:id])
    @image = Image.find_by(page_id: @page.id)
  end

  # GET /pages/new
  def new
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
    @page = Page.friendly.find(params[:id])
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)

    if @page.save
      redirect_to root_path, notice: 'Page was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    if @page.update(page_params)
		flash.alert = true
      redirect_to :back, notice: 'Page was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    redirect_to root_path, notice: 'Page was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_page
    @page = Page.friendly.find(params[:id])
  end

  def get_images
    @images = Image.where.not(page_id: nil)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def page_params
    params.require(:page).permit(:title, sections_attributes: [:id, :title, :content, :order, :page_id, :image, :_destroy], images_attributes: [:id, :file, :alt, :hint, :section_id, :page_id, :_destroy])
  end
end
