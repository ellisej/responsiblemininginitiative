class SectionsController < ApplicationController
  before_action :set_section, only: [:show, :edit, :update, :destroy]

  def index
    @sections = Section.all
  end

  def show
  end

  def new
    @section = Section.new
  end

  def create
    @section = Section.new(section_params)
    if @section.save
      redirect_to @section, notice: 'Section was successfully created!'
    else
      render :new, notice: 'Section was not able to be created!'
    end
  end

  def edit
  end

  def update
    if @section.update(section_params)
      redirect_to :back, notice: 'Section was successfully updated!'
    else
      render :new, notice: 'Section was not able to be updated!'
    end
  end

  def destroy
    @section.destroy
    redirect_to :back
  end

  private
    
    def set_section
      @section = Section.find(params[:id])
    end

    def section_params
      params.require(:section).permit(:title, :content, :order, :page_id)
    end
end
