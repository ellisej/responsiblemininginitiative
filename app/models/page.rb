class Page < ActiveRecord::Base
  extend FriendlyId
  has_many :sections
  has_many :images
  validates_presence_of :title

  friendly_id :title, use: [:slugged,:finders]

  accepts_nested_attributes_for :sections, allow_destroy: true
  accepts_nested_attributes_for :images, allow_destroy: true

  def to_param
    slug
  end

end
