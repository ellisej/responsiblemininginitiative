# spec/controllers/pages_controller.rb

describe 'PagesController' do

	it "creates a page if admin" do
		
	end

	it "will not create a page if not admin" do

	end

	it "edits a page's content if admin" do

	end

	it "will not edit a page's content if not admin" do

	end

	it "deletes a page if admin" do

	end

	it "will not delete if not admin" do

	end

end
