require 'spec_helper'
require 'rails_helper'
require 'capybara'

describe "Admin Login: " do
	before do
		visit '/admins/sign_in'
	end

	it "Should have title" do
		expect(page).to have_title("RMI Admin Login")
	end

	it "Should have Log in text" do
		within("h2") do
			expect(page).to have_content("Log in")
		end
	end

	it "Should have Email and Password fields" do
		expect(page).to have_content("Email")
		expect(page).to have_content("Password")
	end

	it "Should have login button" do
		click_button("Log in")
	end

	it "Test email field" do
		fill_in("admin_email", with: 'text')
	end

	it "Test password field" do
		fill_in("admin_password", with: 'text')
	end

	it "Test Login invalid" do
		fill_in("admin_email", with: 'abc')
		fill_in("admin_password", with: 'abc')
		find_button("Log in").click
		expect(page).to have_title("RMI Admin Login")
	end

	it "Test Login valid" do
		fill_in("admin_email", with: 'admin@uwec.edu')
		fill_in("admin_password", with: 'admin12345')
		find_button("Log in").click
		expect(page).to have_title("RMI")
	end
end
