require 'spec_helper'
require 'rails_helper'
require 'capybara'

describe "Full App: " do
	before do
		Capybara.current_driver = :webkit
	end

	it "Admin login" do
		visit root_path
		expect(page).to have_title("RMI")
		visit '/#mining-life-cycle'
		click_link("Administrator Login")
		expect(page).to have_content("Log in")
		fill_in("admin_email", with: 'admin@uwec.edu')
		fill_in("admin_password", with: 'admin12345')
		click_button("Log in")
		expect(page).to have_content("RMI")
	end

	it "Submit suggestion" do	
		visit '/suggestions/new'
		fill_in("suggestion_name", with: 'test name')
		fill_in("suggestion_email", with: 'email@email.com')
		fill_in("suggestion_content", with: 'test suggestions')
		click_button("Submit")
		page.driver.browser.accept_js_confirms
		expect(page).to have_content("Thank you")
	end

	it "Link to login page" do
		visit '/suggestions'
		expect(page).to have_content("Log in")
	end

	it "Add section" do
		
	end

	it "Mining Life Cycle title" do
		visit root_path
		expect(page).to have_title("RMI")
		visit '/#mining-life-cycle'
		within('div.title-overlay') do
			expect(page).to have_css("h1")
		end
	end
end
