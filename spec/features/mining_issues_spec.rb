require 'spec_helper'
require 'rails_helper'
require 'capybara'

describe "Mining Issues: " do
	before do
		visit '/#mining-issues'
	end

	it "Should have title" do
		expect(page).to have_title("RMI")
	end

	it "Should have links to pages" do
		expect(page).to have_link("RMI")
		expect(page).to have_link("Mining Life Cycle")
		expect(page).to have_link("Commodity Types")
		expect(page).to have_link("Mining Issues")
		expect(page).to have_link("Societal Impacts")
		expect(page).to have_link("Regulatory Framework")
		expect(page).to have_link("Suggestions")
		expect(page).to have_link("Administrator Login")
	end

	it "Should be able to click links" do
		click_link("RMI")
		click_link("Mining Life Cycle")
		click_link("Commodity Types")
		click_link("Mining Issues")
		click_link("Societal Impacts")
		click_link("Regulatory Framework")
		click_link("Suggestions")
		click_link("Administrator Login")
	end

	it "Should have nav bar and button" do
		expect(page).to have_css("nav.navbar")
	   find("button.navbar-toggle.collapsed").click
	   find("button.navbar-toggle").click
	end

	it "Should have h1 title" do
		within('div.title-overlay') do
			expect(page).to have_css("h1")
		end
	end

	it "Should have carousel" do
		expect(page).to have_css("div.carousel")
	end

	it "Should have copyright" do
		expect(page).to have_content("Copyright")
		expect(page).to have_content("All Rights Reserved")
	end
end
