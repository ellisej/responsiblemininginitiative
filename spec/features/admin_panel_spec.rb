require 'spec_helper'
require 'rails_helper'
require 'capybara'

describe "Admin Panel: " do
	before :each do
		visit '#/admin_panel'
	end
	it "Should have title" do
 		expect(page).to have_title("RMI")
 	end 	
	it "Should have links to pages" do 		
		expect(page).to have_link("RMI")
 		expect(page).to have_link("Mining Life Cycle")
 		expect(page).to have_link("Commodity Types")
 		expect(page).to have_link("Mining Issues")
 		expect(page).to have_link("Societal Impacts")
 		expect(page).to have_link("Regulatory Framework")
 		expect(page).to have_link("Suggestions")
 		expect(page).to have_link("Administrator Login")
 	end
	
	it "Should be able to click links" do
		click_link("RMI")
		click_link("Mining Life Cycle")
		click_link("Commodity Types")
		click_link("Mining Issues")
		click_link("Societal Impacts")
		click_link("Regulatory Framework")
		click_link("Suggestions")
		click_link("Administrator Login")
	end

	it "Should have h1 title" do
		within('div.title-overlay') do
			expect(page).to have_css("h1")
		end
	end

	it "Should have buttons" do 
		page.find('button').click
	end
	
	it "Should have 'Submit' button" do 
		have_submit_button("Submit")
	end
end

