require 'faker'

FactoryGirl.define do
	factory :section do |s|
    s.title { Faker::Hipster.sentence(3) }
    s.content { Faker::Hipster.paragraph }
    s.order { Faker::Number.number(2) }
    s.page_id { Faker::Number.number(2) }
    s.created_at { Faker::Time.backward(14) } 
    s.updated_at { Faker::Time.backward(7) }
    #s.string   "image_file_name"
    #s.string   "image_content_type"
    s.image_file_size  { Faker::Number.number(6) } 
    #s.datetime "image_updated_at"
	end
end
