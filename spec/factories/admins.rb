require 'faker'

FactoryGirl.define do
  factory :admin do |a|
    a.email { Faker::Internet.email('Nancy') }
    a.password { Faker::Internet.password(8) }
  end
end