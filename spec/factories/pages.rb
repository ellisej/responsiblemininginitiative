require 'faker'

FactoryGirl.define do
	factory :page do |p|
		p.title { Faker::Hipster.sentence(3) }
	end
end
