require 'spec_helper'
require 'rails_helper'
require 'support/factory_girl'

describe Admin do
  it "should have a factory" do
    expect(create(:admin)).to be_valid
  end
  it "is invalid without an email" do
    expect{create(:admin, email: nil)}.to raise_error(ActiveRecord::RecordInvalid)
  end
  it "is invalid without an encrypted password" do
    expect{create(:admin, password: nil)}.to raise_error(ActiveRecord::RecordInvalid)
  end
end