require 'spec_helper'
require 'rails_helper'
require 'support/factory_girl'

describe Page do
	it "should have a factory" do
    expect(create(:page)).to be_valid
	end
	it "is invalid without a title" do 
		expect{create(:page, title: nil)}.to raise_error(ActiveRecord::RecordInvalid)
	end
end
