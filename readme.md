## README
* Ruby version: `2.3.0`
* Rails version: `4.2.4`

* System dependencies: 
  - linux server: `ngrok`
  - `Nodejs` or `therubyracer` gem

* Database
  - MySql2 staging on Pegasus

* Deployment instructions
  - Capybara script for automatic deployments
