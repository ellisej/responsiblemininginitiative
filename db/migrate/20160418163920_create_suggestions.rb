class CreateSuggestions < ActiveRecord::Migration
  def change
    create_table :suggestions do |t|
      t.text :content
      t.string :email
      t.string :name

      t.timestamps null: false
    end
  end
end
