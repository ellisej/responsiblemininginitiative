class AddPageBannerRelationToImage < ActiveRecord::Migration
  def change
      add_column :images, :page_id, :integer
      add_foreign_key :images, :pages
  end
end
