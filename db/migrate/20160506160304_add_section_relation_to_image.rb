class AddSectionRelationToImage < ActiveRecord::Migration
  def change
    add_reference :images, :section, index: true, foreign_key: true
  end
end
