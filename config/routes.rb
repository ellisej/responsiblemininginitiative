Rails.application.routes.draw do
  devise_for :admins, controllers: {
    sessions: 'admins/sessions'
  }

  post '/tinymce_assets' => 'images#create'

  # Home Page
  root 'pages#home'

  # Posts
  resources :posts

  # RESTful routes
  resources :pages, except: [:index] do
    member do
      get 'admin_panel'
    end
  end
  resources :suggestions, except: [:show, :edit]

end
